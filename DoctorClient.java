import java.util.Scanner;

abstract class Doctor {
    public abstract void treatPatient();
}

class Ortho extends Doctor {
    public void conductXRay (){
        System.out.println("Conducting x-ray");
    }

    public void conductCTScan (){
        System.out.println("Conducting ct-scan");
    }

    public void treatPatient(){
        this.conductXRay();
        this.conductCTScan();
    }
}

class Padietrician extends Doctor {
    public void treatKids (){
        System.out.println("Treating Kids");
    }

    
    public void treatPatient(){
        this.treatKids();
    }
}

class Dentist extends Doctor {
    public void toothExtraction (){
        System.out.println("Tooth extraction");
    }

    public void treatPatient(){
        this.toothExtraction();
    }
}
class Optholmologist extends Doctor {
    public void performEyeTest (){
        System.out.println("Perform eye test");
    }
    public void treatPatient(){
        this.performEyeTest();
    }
}

public class DoctorClient {
    public static void main(String[] args) throws Exception{
        System.out.println( "Please enter your option::");
        System.out.println( "1 -> Ortho");
        System.out.println( "2 -> Dentist");
        System.out.println( "3 -> Padietric");
        System.out.println( "4 -> Optholmologist");

        Scanner sc = new Scanner (System.in);
        int option = sc.nextInt();
        Doctor doctor = null;
        switch (option) {
            case 1: 
                doctor = new Ortho();
                break;
            case 2: 
                doctor = new Dentist();
                break;
            case 3: 
                doctor = new Padietrician();
                break;
            case 4: 
                doctor = new Optholmologist();
                break;
            default: 
                doctor = new Padietrician();
                break;

        }
        doctor.treatPatient();
        sc.close();
    }
}
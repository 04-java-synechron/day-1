import java.io.Serializable;
import java.util.*;

public class ListDemo {
    public static void main(String[] args) {
        Collection<String> strList = new HashSet<>();
        strList.add("one");
        strList.add("one");
        strList.add("three");
        strList.add("four");

        System.out.println("The size of list is " + strList.size());
    }
}

class User implements Serializable{
    private String firstName;
    private String lastName;

}
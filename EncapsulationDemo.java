public class SavingsAccount {
    //hidden data
    private double accountBalance;
    private Customer customer;
    private Address address;

    public SavingsAccount(Customer customer){
        this.customer = customer;
    }

    public void setAddress(Address address ){
        this.address = address;
    }

    public double deposit(double amount ){
        this.accountBalance += amount;
        return this.accountBalance;
    }

    public double withdraw(double amount) {
        if((this.accountBalance - amount) > 0) {
            this.accountBalance -= amount;
            return amount;
        }
        return 0;
    }

    public double getAccountBalance(){
        return this.accountBalance;
    }
}

class Address {
    private String city;
    private String zip;
    private String street;
}

class Customer {
    private String firstName;
    private String panNumber;
}
class Super {
    public int intVar = 100;

    public void display() {
        System.out.println("The value of i is "+ this.intVar);
    }

    public static void staticMethod (){
        System.out.println( " Stateic method from parent class");
    }

}

class Child extends Super {

    int intVar = 45;

    @Override
    public void display() {
        System.out.println("The value of i is "+ this.intVar);
    }

    
    public static void staticMethod (){
        System.out.println( " Stateic method from parent class");
    }

    public static void main(String[] args) {
        Child child = new Child ();
        System.out.println("The value of i is "+ child.intVar);
        child.display();
        
    }
}
public class ArrayDemo {
    /*
      1. Fixed size
      2. contigous block of memory
      3. Similar data types 
      4. Index based 
      5. Index starts with 0
      6. has only one length property
      7. default values of array 
           number - 0
           char = ''
           boolean = false
           object = null
    */

    public static void main(String[] args) {
        //array declaration
        int[] array1 = new int[10];
        int array[] = new int[]{11,22,33,44};
        int [] array2 = {22, 33, 44, 55};

        int[][] twoDim = {{22,33,}, {44,55}, {44,32}};

        int[][] twoDim2 = new int[10][10];
        
        String users[] = new String[] {"Vinay", "mahesh", "Kumar"};
        array[1] = 0;
        users[1] = null;
    }
}
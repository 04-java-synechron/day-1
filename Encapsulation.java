/**
 * 
 * @author Pradeep
 * @version 1.0
 * @since 1.0
 */
public final class EncapsulationDemo {

    /**
     * documentation comments
     * @return void
     * @arg args - string arguments
     * 
     */
    public static void main(String[] args) {
        //variable naming rules
        /*
          multi line comment
          1. cannot start with number
          2. Alphanumeric are allowed
          3. only _ and $ are allowed special characters
          4. should not be a keyword
        */

        //conventions
        /*
          1. Camelcase 
          2. For classes, Interfaces, Enum use Capital first letter
          3. For methods, variable use smll first letter
          4. Give descriptive names
              no i,j,k 
        */

        int value = 3_50_000;
    }
}
public class ListDemo {
    public static void main(String[] args) {

        //imperative style of programming
        List<String> names = new ArrayList<>();
        names.add("Vinay");
        names.add("Ramesh");
        names.add("Suresh");
        names.add("Rakesh");
        names.add("Meena");
        names.add("Manish");

        List<String> newNames = Arrays.asList("Harish", "Seema", "Sunil", "Vikram");
        List<String> listOfNames = List.of("Harish", "Seema", "Sunil", "Vikram");

    }
}
import java.io.*;

public class ExceptionDemo {
    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new FileReader(new File("D:\\data.txt")))){
            boolean flag = true;
            while (flag){
                String line = reader.readLine();
                if (line == null){
                    flag = false;
                    continue;
                }
                System.out.println(line);
            }
        } catch(FileNotFoundException exception){
            System.out.println("Invalid file name or location");
        } catch(Exception exception){
            System.out.println("Exception caught "+ exception.getMessage());
        }
    }
}
public class VariableDemo {

    public static void main(String[] args) {
        byte value = (byte)130; 
        System.out.println("Value is "+ value);   

        Object reference = new LinkedList();
        //run time - Class cast exception
        if (reference instanceof ArrayList) {
            ArrayList list = (ArrayList) reference;
        }
        
    }
}
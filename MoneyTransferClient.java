interface MoneyTransfer {
    void transferMoney (String from, String to, double amount, String notes);
}

class GooglePay implements MoneyTransfer {
    public void transferMoney (String from, String to, double amount, String notes){
        System.out.println("Money transfer using Google pay");
    }
}
class PhonePay implements MoneyTransfer {
    public void transferMoney (String from, String to, double amount, String notes){
        System.out.println("Money transfer using Phone pay");
    }
}

class PayTM implements MoneyTransfer {
    public void transferMoney (String from, String to, double amount, String notes){
        System.out.println("Money transfer using PayTM");
    }
}

public class MoneyTransferClient {
    public static void main(String[] args) {
        MoneyTransfer app = new GooglePay();
        app.transferMoney("Kishore", "Vinod", 45_000, "towards Rental agreement");
    }
}
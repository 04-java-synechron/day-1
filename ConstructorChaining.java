class GrandParent {

    private String city;
    private String state;
    
    public GrandParent (String city, String state){
        this.city = city;
        this.state = state;
    }
}

class Parent extends GrandParent {
    public Parent (String city, String state) {
        super(city, tate);
    }
}

class Child extends Parent {
    public Child(String city, String state){
        super(city, state);
    }

    
}

public class ConstructorChaining {
    public static void main(String[] args) {
        Child child = new Child ();
    }
}